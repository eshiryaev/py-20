from abc import ABC, abstractmethod


class Animal(ABC):
    name = ''
    is_hungry = True
    type_of_animal = ''
    kind_of_animal = ''
    age = 1
    voice = ''
    weight = 0

    def __init__(self, name, type_of_animal, voice, weight):
        self.name = name
        self.type_of_animal = type_of_animal
        self.voice = voice
        self.weight = weight

    @abstractmethod
    def collect(self):
        pass

    def feed(self):
        self.is_hungry = False

        return self.is_hungry

    def get_voice(self):
        return self.voice


class Bird(Animal):
    kind_of_animal = 'bird'
    give_eggs = 5

    def collect(self):
        return self.give_eggs


class Cow(Animal):
    kind_of_animal = 'hoofed'
    type_of_animal = 'cow'
    give_milk = '12 л'

    def __init__(self, name, weight, voice='Muuuuuu'):
        super().__init__(name, self.type_of_animal, voice, weight)

    def collect(self):
        return self.give_milk


class Sheep(Animal):
    kind_of_animal = 'hoofed'
    type_of_animal = 'sheep'
    give_wool = True

    def __init__(self, name, voice, weight):
        super().__init__(name, self.type_of_animal, voice, weight)

    def collect(self):
        return self.give_wool


class Goat(Animal):
    kind_of_animal = 'hoofed'
    type_of_animal = 'goat'
    give_milk = '3 л'

    def __init__(self, name, voice, weight):
        super().__init__(name, self.type_of_animal, voice, weight)

    def collect(self):
        return self.give_milk


animals = []

animals.append(Bird('Серый', 'Goose', 'Ga-ga', 3))
animals.append(Bird('Белый', 'Goose', 'Ga-ga-ga', 4))
animals.append(Bird('Ко-ко', 'Chicken', 'Kukarekuuuuu', 1))
animals.append(Bird('Кукареку', 'Chicken', 'Kukarekuuuuu kudah', 2))
animals.append(Bird('Кряква', 'Duck', 'Kryaaaak', 5))
animals.append(Cow('Манька', 200))
animals.append(Sheep('Барашек', 'Beeeeee', 80))
animals.append(Sheep('Кудрявый', 'Beeeeee be', 78))
animals.append(Goat('Рога', 'Bee', 50))
animals.append(Goat('Копыта', 'Beee', 55))

# устроим переключку
for animal in animals:
    print(animal.get_voice())

# покормим всех и убедимся что никто не голоден
for animal in animals:
    print(animal.feed())

# ну и дивиденды соберем
for animal in animals:
    print(animal.collect())


summ_weight = 0
biggest = {
    'weight': 0,
    'name': ''
}

for value in animals:
    if biggest['weight'] <= value.weight:
        biggest['name'] = value.name
        biggest['weight'] = value.weight

    summ_weight += value.weight

print('Самое тяжёлое животное - "{}" весит {}кг'.format(biggest['name'], biggest['weight']))
print('Общий вес: {}кг'.format(summ_weight))

