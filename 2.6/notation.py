def polish_notation(str):
    str_list = str.split(' ')

    try:
        assert int(str_list[1]) > 0

        assert int(str_list[2]) > 0

        if str_list[0] == '+':
            return int(str_list[1]) + int(str_list[2])
        elif str_list[0] == '-':
            return int(str_list[1]) - int(str_list[2])
        elif str_list[0] == '*':
            return int(str_list[1]) * int(str_list[2])
        elif str_list[0] == '/':
            return int(str_list[1]) / int(str_list[2])
    except AssertionError:
        print('Аргументы должны быть положительными числами')
    except:
        print('Произошла непредвиденная ошибка')

    return None


print(
    polish_notation(
        input('Введите операцию:')
    )
)
