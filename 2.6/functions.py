documents = [
    {"type": "passport", "number": "2207 876234", "name": "Василий Гупкин"},
    {"type": "invoice", "number": "11-2", "name": "Геннадий Покемонов"},
    {"type": "insurance", "number": "10006", "name": "Аристарх Павлов"},
    {"type": "invoice", "number": "11-23"},
    {"type": "invoice", "number": "11-27"},
]


# команда, которая спросит номер документа и выведет имя человека, которому он принадлежит
def show_person(document_number):
    try:
        for elem in documents:
            if elem['number'] == document_number:
                return elem['name']

    except KeyError:
        print('Ключа name не существует у документа')
        return None

    return 'Не найдено'


while True:
    cmd = input('Ввведите команду:')

    if cmd == 'q':
        break
    elif cmd == 'p':
        print('Имя:', show_person(input('Введите номер документа:')))
