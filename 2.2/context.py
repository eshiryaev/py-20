import json
import time


def find_popular_word(rss):
    popular_dict = {}

    for item in rss['rss']['channel']['items']:
        item_list = item['description'].lower().split(' ')

        for word in item_list:
            if len(word) > 6:
                if popular_dict.get(word):
                    popular_dict[word] += 1
                else:
                    popular_dict[word] = 1

    return sorted(popular_dict, key=popular_dict.get, reverse=True)


class ContextManager:
    def __init__(self, path):
        print('\nВремя запуска кода: {}'.format(time.ctime()))
        self.file = open(path, encoding='utf8')

    def __enter__(self):
        try:
            json_data = json.load(self.file)
        except:
            json_data = {}

        data = find_popular_word(json_data)

        return data

    def __exit__(self, *args):
        self.file.close()
        print('\nВремя окончания работы кода: {}'.format(time.ctime()))
        print('\nВремя выполнения кода: {} сек\n'.format(time.time() - start_time))


if __name__ == '__main__':
    start_time = time.time()

    with ContextManager('newsafr.json') as data_file:
        print(data_file)
