from urllib.parse import urlencode
import requests

# данные для получения токена
APP_ID = 6777507
AUTH_URL = 'https://oauth.vk.com/authorize?'
auth_data = {
    'client_id': APP_ID,
    'display': 'page',
    'redirect_uri': '',
    'response_type': 'token',
    'scope': 'status, friends ',
    'v': '5.92'
}

# ссылка возвращает в адресной строке браузера токен
print(AUTH_URL + urlencode(auth_data))
print('+' * 40, '\n')


class User:
    METHOD_URL = 'https://api.vk.com/method/'

    def __init__(self, id, token):
        self.token = token
        self.id = id

    # just for fun
    def get_profile(self):
        params = {
            'access_token': self.token,
            'user_ids': self.id,
            'v': '5.92',
        }

        response = requests.get(self.METHOD_URL + 'users.get', params)

        return response.json()

    def get_mutual_friends(self, id_1, id_2):
        params = {
            'access_token': self.token,
            'v': '5.92',
            'source_uid': id_1,
            'target_uid': id_2
        }

        response = requests.get(self.METHOD_URL + 'friends.getMutual', params)

        return response.json()

    def __and__(self, other):
        res = self.get_mutual_friends(self.id, other.id)
        friends = []

        for user in res['response']:
            friends.append(User(user, self.token))

        return friends

    def __str__(self):
        return 'https://vk.com/id' + str(self.id)


if __name__ == "__main__":
    token = 'e08cb729cc197d2d1f4133b4a95a67b46039ad50b943c6d2eb83af803aabd977c48cec5a129b6ccab9a04'

    slava = User(54900572, token)
    german = User(347655652, token)

    print(german & slava)
    print(slava)
    print(german)
