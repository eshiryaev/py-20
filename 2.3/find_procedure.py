# Задание
# мне нужно отыскать файл среди десятков других
# я знаю некоторые части этого файла (на память или из другого источника)
# я ищу только среди .sql файлов
# 1. программа ожидает строку, которую будет искать (input())
# после того, как строка введена, программа ищет её во всех файлах
# выводит список найденных файлов построчно
# выводит количество найденных файлов
# 2. снова ожидает ввод
# поиск происходит только среди найденных на этапе 1
# 3. снова ожидает ввод
# ...
# Выход из программы программировать не нужно.
# Достаточно принудительно остановить, для этого можете нажать Ctrl + C

# Пример на настоящих данных

# python3 find_procedure.py
# Введите строку: INSERT
# ... большой список файлов ...
# Всего: 301
# Введите строку: APPLICATION_SETUP
# ... большой список файлов ...
# Всего: 26
# Введите строку: A400M
# ... большой список файлов ...
# Всего: 17
# Введите строку: 0.0
# Migrations/000_PSE_Application_setup.sql
# Migrations/100_1-32_PSE_Application_setup.sql
# Всего: 2
# Введите строку: 2.0
# Migrations/000_PSE_Application_setup.sql
# Всего: 1

# не забываем организовывать собственный код в функции

import os
import glob

migrations = 'Migrations'


def find_str_in_file(file_name, search_value):
    f = open(file_name, encoding='utf8')
    file_str = f.read()

    if file_str.find(search_value) > 0:
        return True

    return False


def search(search_value, list_of_files):
    indicator = 0
    temp_list = list()

    for file_name in list_of_files:
        if find_str_in_file(file_name, search_value):
            indicator += 1
            temp_list.append(file_name)

            print(file_name)

    print('Найдено элементов: {} из {} файлов'.format(indicator, len(list_of_files)))

    return temp_list


if __name__ == '__main__':
    os.chdir(migrations)
    migrations_list = glob.glob('*.sql')


    while True:
        result_list = search(input('Поиск по файлам: '), migrations_list)
        migrations_list = result_list[:]

        if not(len(migrations_list)):
            break
