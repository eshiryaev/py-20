# Задание 2

from cook_book import make_cook_book


def denormalize_dish(dish):
    temp_list = []

    for elem in dish:
        # приводим к формату: 'Картофель': {'measure': 'кг', 'quantity': 2}
        custom_el = {
            elem.pop('ingridient_name'): elem
        }

        temp_list.append(custom_el)

    return temp_list


def denormalize_cook_book(dishes):
    temp_list = []
    my_cook_book = make_cook_book()

    # создаем список из переданных блюд в удобном для нас формате
    for element in dishes:
        temp_list.extend(denormalize_dish(my_cook_book[element]))

    return temp_list


def get_shop_list_by_dishes(dishes, person_count):
    our_dishes = denormalize_cook_book(dishes)
    final_dict = {}

    # формируем итоговый сгруппированный список продуктов
    for element in our_dishes:
        for key, value in element.items():
            if not key in final_dict:
                final_dict[key] = value
            else:
                final_dict[key]['quantity'] += value['quantity']

    # подсчитываем кол-во ингридиентов на `person_count` человек
    for element in final_dict:
        final_dict[element]['quantity'] *= person_count

    return final_dict


print(
    get_shop_list_by_dishes(['Запеченный картофель', 'Омлет'], 2)
)
