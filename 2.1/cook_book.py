# Задание 1


def make_cook_book():
    cook_book = {}
    f = open('./file.txt', encoding='utf8')
    dishes = f.read().split('\n\n')

    for dish in dishes:
        temp_dish = dish.split('\n')

        # temp_dish[2:2 + int(temp_dish[1])]
        # срез массива cо 2 индекса по (2 + кол-во ингридиентов)
        cook_book[temp_dish[0]] = make_dish(temp_dish[2:2 + int(temp_dish[1])])

    return cook_book


def make_dish(dish):
    ingridients = []

    for ingridient in dish:
        ingridients.append(make_ingridient(ingridient))

    return ingridients


def make_ingridient(ingridient):
    # Помидор | 2 | шт
    temp_list = ingridient.split(' | ')

    return {
        'ingridient_name': temp_list[0],
        'quantity': int(temp_list[1]),
        'measure': temp_list[2]
    }
