import requests


def translate_text(text, lang_from, lang_to):
    url = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
    key = 'trnsl.1.1.20161025T233221Z.47834a66fd7895d0.a95fd4bfde5c1794fa433453956bd261eae80152'

    params = {
        'key': key,
        'lang': '{}-{}'.format(lang_from, lang_to),
        'text': text,
    }

    response = requests.get(url, params=params).json()

    return ' '.join(response.get('text', []))


def translate_file(text_file_path, res_file_path, lang_from, lang_to='ru'):
    with open(text_file_path, encoding='utf8') as datafile:
        result = translate_text(datafile.read(), lang_from, lang_to)

    with open(res_file_path, 'w', encoding='utf8') as res_file:
        res_file.write(result)

        return 'Перевод {} выполнен и находится в файле {}'.format(text_file_path, res_file_path)


def translate():
    files = {
        'de': 'DE.txt',
        'es': 'ES.txt',
        'fr': 'FR.txt',
    }

    for key, value in files.items():
        translate_file(value, key + '_translate.txt', key, 'ru')


if __name__ == '__main__':
    translate()
