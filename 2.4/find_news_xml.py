import xml.etree.ElementTree as ET


def open_news():
    tree = ET.parse('newsafr.xml', ET.XMLParser(encoding='utf-8'))

    return tree.getroot()


def find_popular_word(tree):
    items = tree.findall('channel/item')

    popular_dict = {}

    for item in items:
        item_desc = item.find('description')

        for word in item_desc.text.lower().split(' '):
            if len(word) > 6:
                if popular_dict.get(word):
                    popular_dict[word] += 1
                else:
                    popular_dict[word] = 1

    for word in sorted(popular_dict, key=popular_dict.get, reverse=True)[:10]:
        print(word, ':', popular_dict[word])


print(
    find_popular_word(open_news())
)