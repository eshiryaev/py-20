import json
from pprint import pprint


def open_news():
    with open('newsafr.json', encoding='utf8') as datafile:
        return json.load(datafile)


def find_popular_word(rss):
    popular_dict = {}

    for item in rss['rss']['channel']['items']:
        item_list = item['description'].lower().split(' ')

        for word in item_list:
            if len(word) > 6:
                if popular_dict.get(word):
                    popular_dict[word] += 1
                else:
                    popular_dict[word] = 1

    for word in sorted(popular_dict, key=popular_dict.get, reverse=True)[:10]:
        print(word, ':', popular_dict[word])


pprint(
    find_popular_word(open_news())
)
